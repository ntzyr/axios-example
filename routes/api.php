<?php

use Illuminate\Http\Request;
use App\Http\Resources\ArticlesResource;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('articles', 'ArticleController@index');
Route::get('article/{id}', 'ArticleController@show');
Route::delete('article/{id}', 'ArticleController@destroy');
Route::post('article/', 'ArticleController@create');
Route::put('article/{id}', 'ArticleController@update');

Route::get('categories', 'CategoryController@index');

