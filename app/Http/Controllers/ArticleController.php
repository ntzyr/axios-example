<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Resources\ArticleResource;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $orderBy = $request->input('orderBy') ?? 'created_at';
        $order = $request->input('order') ?? 'desc';
        $articles = Article::orderBy($orderBy, $order)->paginate(6);

        return ArticleResource::collection($articles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $article = new Article;
        $article->title = $request->input('title');
        $article->content = $request->input('content');
        if ($article->save()) {
            $article->categories()->attach($request->input('categories'));
            return new ArticleResource($article);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = Article::findOrFail($id);
        if ($article) {
            return new ArticleResource($article);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $article = Article::findOrFail($request->input('id'));

        $article->categories()->detach($article->categories);

        $article->title = $request->input('title');
        $article->content = $request->input('content');
        if ($article->save()) {
            $article->categories()->attach($request->input('categories'));
            return new ArticleResource($article);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::findOrFail($id);

        if ($article->delete()) {
            return new ArticleResource($article);
        }
    }
}
